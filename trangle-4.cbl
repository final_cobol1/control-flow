       IDENTIFICATION DIVISION.
       PROGRAM-ID. triangle-2.
       AUTHOR. KIIRATi.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       
       01  SCR-LINE        PIC X(80) VALUE SPACES.
       01  STAR-NUM        PIC 9(3) VALUE ZEROS.
           88 VALID-STAR-NUM VALUE 0 THRU 80.
       01  INDEX-NUM1       PIC 9(3) VALUE ZEROS.
       01  INDEX-NUM2       PIC 9(3) VALUE ZEROS.

       PROCEDURE DIVISION.
       000-BEGIN.
           MOVE ALL SPACE TO SCR-LINE
           MOVE ALL "*" TO SCR-LINE(1:5)
           DISPLAY SCR-LINE
           MOVE ALL SPACE TO SCR-LINE
           MOVE ALL "*" TO SCR-LINE(2:4)
           DISPLAY SCR-LINE
           MOVE ALL SPACE TO SCR-LINE
           MOVE ALL "*" TO SCR-LINE(3:3)
           DISPLAY SCR-LINE
           MOVE ALL SPACE TO SCR-LINE
           MOVE ALL "*" TO SCR-LINE(4:2)
           DISPLAY SCR-LINE
           MOVE ALL SPACE TO SCR-LINE
           MOVE ALL "*" TO SCR-LINE(5:1)
           DISPLAY SCR-LINE
           GOBACK
           .   

       